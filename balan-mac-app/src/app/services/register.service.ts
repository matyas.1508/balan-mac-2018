import { Injectable } from '@angular/core';
import { AuthService } from '../core/auth.service';

@Injectable({
    providedIn: 'root'
})
export class RegisterService {

    constructor(private auth: AuthService) { }

    submitForm(formData) {
        formData.displayName = `${formData.firstName} ${formData.lastName}`;
        formData.secondPassword = null;

        return this.auth.emailSignup(formData);
    }
}
