import { AuthService } from '../../core/auth.service';
import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-navbar',
    templateUrl: './navbar.component.html',
    styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

    items: Array<Array<Object>> = [
        [
            {
                name: 'Inicio',
                link: '/index'
            },
            {
                name: 'La Empresa',
                link: '/company'
            },
            {
                name: 'Servicio Técnico',
                link: '/technical-service'
            },
            {
                name: 'Productos',
                link: '/products'
            },
            {
                name: 'Contacto',
                link: '/contact'
            }

        ],
        [
            {
                name: 'Registro',
                link: '/register'
            },
            {
                name: 'Inicio de sesión',
                link: '/login'
            }
        ],
        [
            {
                name: 'Perfil',
                link: '/profile'
            }
        ]
    ];

    constructor(public auth: AuthService) { }

    ngOnInit() {
    }

}
