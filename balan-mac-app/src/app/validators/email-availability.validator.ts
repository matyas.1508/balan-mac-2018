import { debounceTime, take, map } from 'rxjs/operators';
import { AbstractControl } from '@angular/forms';
import { AngularFirestore } from 'angularfire2/firestore';

export class EmailAvailabilityValidator {
    static email(afs: AngularFirestore) {
        return (control: AbstractControl) => {
            const email = control.value.toLowerCase();

            return afs.collection('users', ref => ref.where('email', '==', email))
                .valueChanges()
                .pipe(debounceTime(500),
                    take(1),
                    map(arr => arr.length ? { notAvailable: true } : null)
                );
        };
    }
}

