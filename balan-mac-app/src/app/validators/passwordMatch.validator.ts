import { AbstractControl } from '@angular/forms';

export function PasswordMatchValidator(firstControlName, secondControlName) {

    return (AC: AbstractControl) => {
        const firstControlValue = AC.get(firstControlName).value;
        const secondControlValue = AC.get(secondControlName).value;

        if (firstControlValue !== secondControlValue) {
            AC.get(secondControlName).setErrors({ notMatch: true });
        }

        return null;
    };

}
