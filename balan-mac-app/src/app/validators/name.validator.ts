import { AbstractControl } from '@angular/forms';

export function ForbiddenNameValidator(control: AbstractControl) {
    if (control.value) {

        return (control.value.toLowerCase() === 'balanmac') ? { invalidName: true } : null;
    }
}
