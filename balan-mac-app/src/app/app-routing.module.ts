import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthGuard } from './core/auth.guard';

import { HomeComponent } from './user-site/home/home.component';
import { RegisterComponent } from './user-site/register/register.component';
import { LoginComponent } from './user-site/login/login.component';
import { UserProfileComponent } from './user-site/user-profile/user-profile.component';
import { CompanyComponent } from './user-site/company/company.component';
import { TechnicalServiceComponent } from './user-site/technical-service/technical-service.component';
import { ContactComponent } from './user-site/contact/contact.component';

const routes: Routes = [
    { path: 'index', component: HomeComponent},
    { path: 'company', component: CompanyComponent},
    { path: 'technical-service', component: TechnicalServiceComponent},
    // { path: 'products', component: ProductsComponent},
    { path: 'contact', component: ContactComponent},
    { path: 'register', component: RegisterComponent },
    { path: 'login', component: LoginComponent},
    { path: 'profile', component: UserProfileComponent, canActivate: [AuthGuard] },
    { path: '', redirectTo: '/index', pathMatch: 'full' },
    { path: '**', redirectTo: '/index' }
];

@NgModule({
    imports: [RouterModule.forRoot(routes, { useHash: true })],
    exports: [RouterModule]
})
export class AppRoutingModule { }
