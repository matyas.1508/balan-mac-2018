import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SideComponent } from './side/side.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    SideComponent
  ],
  exports: [
    SideComponent
  ]
})
export class SideModule { }
