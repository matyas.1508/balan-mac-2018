import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, AbstractControl, Validators } from '../../../../node_modules/@angular/forms';
import { Router } from '../../../../node_modules/@angular/router';

import { AuthService } from '../../core/auth.service';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss', '../register/register.component.scss']
})
export class LoginComponent implements OnInit {

    loginForm: FormGroup;

    constructor(public auth: AuthService, private fb: FormBuilder, private router: Router) {
        this.createLoginForm();
    }

    ngOnInit() {
    }

    onSubmit(): void {
        this.auth.emailLogin(this.loginForm.controls.email.value, this.loginForm.controls.password.value)
            .then(() => {
                this.router.navigate(['/profile']);
            });
    }

    onProvider(providerName): void {
        this.auth.providerLogin(providerName)
            .then(() => {
                this.router.navigate(['/profile']);
            });
    }

    onClear(): void {
        this.loginForm.reset();
    }

    get email(): AbstractControl { return this.loginForm.get('email'); }
    get password(): AbstractControl { return this.loginForm.get('password'); }

    private createLoginForm(): void {
        this.loginForm = this.fb.group({
            email: ['', [Validators.required, Validators.email]],
            password: ['', [Validators.required, Validators.minLength(6)]],
        });
    }
}
