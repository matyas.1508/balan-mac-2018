import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';
import { Router } from '@angular/router';
import { AngularFirestore } from 'angularfire2/firestore';
import { RegisterService } from '../../services/register.service';
import { ForbiddenNameValidator } from '../../validators/name.validator';
import { PasswordMatchValidator } from '../../validators/passwordMatch.validator';
import { EmailAvailabilityValidator } from '../../validators/email-availability.validator';

@Component({
    selector: 'app-register',
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

    registerForm: FormGroup;

    constructor(private fb: FormBuilder, private _RegisterService: RegisterService, private afs: AngularFirestore, private router: Router) {
        this.createRegisterForm();
    }

    ngOnInit() {
    }

    onSubmit(): void {
        this._RegisterService.submitForm(this.registerForm.value)
            .then(() => {
                this.onClear();
                this.router.navigate(['/profile']);
            });
    }

    onClear(): void {
        this.registerForm.reset();
    }

    get firstName(): AbstractControl { return this.registerForm.get('firstName'); }
    get lastName(): AbstractControl { return this.registerForm.get('lastName'); }
    get email(): AbstractControl { return this.registerForm.get('email'); }
    get password(): AbstractControl { return this.registerForm.get('password'); }
    get secondPassword(): AbstractControl { return this.registerForm.get('secondPassword'); }

    private createRegisterForm(): void {
        this.registerForm = this.fb.group({
            firstName: ['', [Validators.required, Validators.minLength(3), ForbiddenNameValidator]],
            lastName: ['', [Validators.required, Validators.minLength(3), ForbiddenNameValidator]],
            email: ['', [Validators.required, Validators.email], EmailAvailabilityValidator.email(this.afs)],
            password: ['', [Validators.required, Validators.minLength(6)]],
            secondPassword: ['', [Validators.required]]
        }, {
                validator: PasswordMatchValidator('password', 'secondPassword')
            });
    }
}
