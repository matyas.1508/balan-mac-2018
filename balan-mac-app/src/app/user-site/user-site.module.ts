import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { RegisterComponent } from './register/register.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { CompanyComponent } from './company/company.component';
import { TechnicalServiceComponent } from './technical-service/technical-service.component';
import { ContactComponent } from './contact/contact.component';

@NgModule({
    imports: [
        CommonModule,
        ReactiveFormsModule
    ],
    declarations: [
        RegisterComponent,
        UserProfileComponent,
        LoginComponent,
        HomeComponent,
        CompanyComponent,
        TechnicalServiceComponent,
        ContactComponent
    ]
})
export class UserSiteModule { }
