import { environment } from '../environments/environment';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from '../../node_modules/angularfire2/auth';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { CoreModule } from './core/core.module';
import { NavigationModule } from './navigation/navigation.module';
import { SideModule } from './side/side.module';
import { UserSiteModule } from './user-site/user-site.module';

@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        BrowserModule,
        HttpClientModule,
        AppRoutingModule,
        AngularFireModule.initializeApp(environment.firebase),
        AngularFireAuthModule,
        CoreModule,
        NavigationModule,
        SideModule,
        UserSiteModule,
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule { }
