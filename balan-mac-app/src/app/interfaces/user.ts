export interface User {
    uid: string;
    email: string;
    photoURL?: string;
    name?: string;
    lastName?: string;
    displayName?: string;
    favoriteColor?: string;
}
