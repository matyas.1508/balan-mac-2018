import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import * as firebase from 'firebase';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFirestore, AngularFirestoreDocument } from 'angularfire2/firestore';

import { Observable, of } from 'rxjs';
import { switchMap } from 'rxjs/operators';

import { User } from '../interfaces/user';

@Injectable({
    providedIn: 'root'
})
export class AuthService {

    user: Observable<User>;

    constructor(private afAuth: AngularFireAuth, private afs: AngularFirestore, private router: Router) {

        this.user = this.afAuth.authState.pipe(
            switchMap(user => {

                return user ? this.afs.doc<User>(`users/${user.uid}`).valueChanges() : of(null);
            })
        );
    }

    providerLogin(providerName: string) {
        const provider = new firebase.auth[`${providerName}AuthProvider`];

        return this.afAuth.auth.signInWithPopup(provider)
            .then(credential => {
                const user = credential.user;
                const data: User = {
                    uid: user.uid,
                    email: user.email,
                    displayName: user.displayName,
                    photoURL: user.photoURL
                };

                this.updateUserData(user, data);
            })
            .catch(this.providerErrorHandler.bind(this));
    }


    emailSignup(user): Promise<any> {

        return this.afAuth.auth.createUserWithEmailAndPassword(user.email, user.password)
            .then(response => {
                const data: User = {
                    uid: response.user.uid,
                    email: user.email,
                    displayName: user.displayName
                };
                this.updateUserData(response.user, data);
            });
    }

    emailLogin(email, password) {
        return this.afAuth.auth.signInWithEmailAndPassword(email, password);
    }

    logOut() {
        this.afAuth.auth.signOut().then(() => this.router.navigate(['/login']));
    }

    private updateUserData(user, data) {
        // Sets user data to firestore on login
        const userRef: AngularFirestoreDocument<User> = this.afs.doc(`users/${user.uid}`);

        // The true flag need to be active to avoid data overriding
        return userRef.set(data, { merge: true });
    }

    private providerErrorHandler({ email, credential, code }) {
        if (code === 'auth/account-exists-with-different-credential') return this.alreadyExist(email, credential);
    }

    private alreadyExist(email, credential) {
        return this.afAuth.auth.fetchSignInMethodsForEmail(email)
            .then(providersHandler.bind(this));

        function providersHandler(providers) {
            if (providers[0] === 'password') {
                const password = prompt('Este correo electrónico está asociado con otro usuario, vuelva a ingresar su contraseña para vincular sus múltiples cuentas.');
                return this.afAuth.auth.signInWithEmailAndPassword(email, password)
                    .then(() => {
                        this.afAuth.auth.currentUser.linkAndRetrieveDataWithCredential(credential);
                    });
            }
            if (providers[0] === 'google.com') {
                alert('Este correo electrónico está asociado a un usuario de Google, a continuación vuelva a iniciar sesión para vincular sus múltiples cuentas.');
                return this.afAuth.auth.signInWithPopup(new firebase.auth.GoogleAuthProvider)
                    .then(() => {
                        this.afAuth.auth.currentUser.linkAndRetrieveDataWithCredential(credential);
                    });
            }
        }
    }
}
